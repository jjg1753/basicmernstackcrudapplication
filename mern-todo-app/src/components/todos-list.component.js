import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';


//Implementing TodoProp Component logic
//Printing out a simple row in the output table
const Todo = props => (
  <tr>
    <td className={props.todoProp.todo_completed ? 'completed' : ''}>{props.todoProp.todo_description}</td>
    <td className={props.todoProp.todo_completed ? 'completed' : ''}>{props.todoProp.todo_responsible}</td>
    <td className={props.todoProp.todo_completed ? 'completed' : ''}>{props.todoProp.todo_priority}</td>
    <td className={props.todoProp.todo_completed ? 'completed' : ''}>{props.todoProp.todo_date}</td>
    <td className={props.todoProp.todo_completed ? 'completed' : ''}>
      <Link to={"/edit/"+props.todoProp._id}>Edit</Link>
    </td>
    <td className={props.todoProp.todo_completed ? 'completed' : ''}>
      <Link to={"/delete/"+props.todoProp._id}>Delete</Link>
    </td>
  </tr>
)

export default class TodoList extends Component{

  constructor(props) {
    super(props);
    this.state = {todos: []};
  }

  //sending a request to back-end to get the documents saved in database to be displayed
  componentDidMount(){
    axios.get('http://localhost:4000/todos')
      .then(response => {
        //console.log(response.data);
        this.setState({todos: response.data});
      })
      .catch(function(error) {
        console.log(error);
      })
  }

  todoList() {
    return this.state.todos.map(function(currentTodo, i) {
      return <Todo todoProp={currentTodo} key = {i} />;
    });
  }

  componentDidUpdate() {
    axios.get('http://localhost:4000/todos')
      .then(response => {
        //console.log(response.data);
        this.setState({todos: response.data});
      })
      .catch(function(error) {
        console.log(error);
      })
  }

  render(){
    return(
        <div>
          <h3> Todo List </h3>
          <table className="table table-striped" style = {{marginTop: 30}}>
            <thead>
              <tr>
                <th>Description</th>
                <th>Responsible</th>
                <th>Priority</th>
                <th>Date and Time</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              { this.todoList() }
            </tbody>
          </table>
        </div>
    )
  }
}
