//Will be using axios library to send HTTP requests from the front-end
//to the backend. Basically axios will be used for front-end and back-end connectivity.


import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import {BrowserRouter as Router, Route, Link} from "react-router-dom";
import CreateTodo from "./components/create-todo.component"
import EditTodo from "./components/edit-todo.component"
import TodoList from "./components/todos-list.component"
import DeleteTodo from "./components/delete-todo.component"

//Logo Import Statement
import logo from "./myLogo.png"

function App() {
  return (
    <Router>
      <div className="container-fluid">
        <nav className="navbar navbar-expand-lg navbar-light bg-light navbar-inverse">
          <div className="navbar-header">
          <a className="navbar-brand" href="localhost:3000">
            <img src={logo} width="30" height="30" alt="Todo CRUD Application"></img>
          </a>
          <Link to="/" className="navbar-brand">Mern-Stack ToDo App</Link>
          </div>
            <ul className="nav navbar-nav mr-auto">
              <li>
                <Link to="/" className="nav-link">Home Page</Link>
              </li>
              <li>
                <Link to="/create" className="nav-link">Create Todo</Link>
              </li>
            </ul>
        </nav>
        <Route path="/" exact component={TodoList} />
        <Route path="/edit/:id" component={EditTodo} />
        <Route path="/create" component={CreateTodo} />
        <Route path="/delete/:id" component={DeleteTodo} />
        </div>
    </Router>
  );
}

export default App;
