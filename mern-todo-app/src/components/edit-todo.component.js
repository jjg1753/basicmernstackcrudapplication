import React, {Component} from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

export default class EditTodo extends Component{

  constructor(props) {
    super(props);

    this.onChangeToDoDescription = this.onChangeToDoDescription.bind(this);
    this.onChangeToDoResponsible = this.onChangeToDoResponsible.bind(this);
    this.onChangeToDoPriority = this.onChangeToDoPriority.bind(this);
    this.onChangeToDoCompleted = this.onChangeToDoCompleted.bind(this);
    this.onChangeToDoDate = this.onChangeToDoDate.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      todo_description: '',
      todo_responsible: '',
      todo_priority: '',
      todo_completed: false,
      todo_date: ''
    }
  }


  onChangeToDoDescription(e) {
    this.setState({
      todo_description: e.target.value
    });
  }

  onChangeToDoResponsible(e) {
    this.setState({
      todo_responsible: e.target.value
    });
  }

  onChangeToDoPriority(e) {
    this.setState({
      todo_priority: e.target.value
    });
  }

  onChangeToDoCompleted(e) {
    this.setState({
      todo_completed: !this.state.todo_completed
    });
  }

  onChangeToDoDate(e) {
    this.setState({
      todo_date: e.target.value
    });
  }

  onSubmit(e) {
    e.preventDefault();

    //Creating the updated object with all the state values
    const updatedTodo = {
      todo_description: this.state.todo_description,
      todo_responsible: this.state.todo_responsible,
      todo_priority: this.state.todo_priority,
      todo_completed: this.state.todo_completed,
      todo_date: this.state.todo_date
    };

    //Sending the updated object as a post request
    axios.post('http://localhost:4000/todos/update/'+this.props.match.params.id, updatedTodo)
      .then(res => console.log(res.data));

    //Redirecting the page back to the list of todo page(homepage)
    this.props.history.push('/');

  }

  componentDidMount() {
    //Extracting the id from props to get a single record from the database
    axios.get('http://localhost:4000/todos/' + this.props.match.params.id)
      .then(response => {
        console.log(response.data);
        this.setState({
          todo_description: response.data.todo_description,
          todo_responsible: response.data.todo_responsible,
          todo_priority: response.data.todo_priority,
          todo_completed: response.data.todo_completed,
          todo_date: response.data.todo_date
        })
      })
      .catch(function(error) {
        console.log(error);
      })
  }

  render(){
    return(
      <div>
        <h3> Update this todo item and hit submit </h3>
        <form onSubmit={this.onSubmit}>
        <div className="form-group">
          <label>Description: </label>
          <input type="text"
                 className="form-control"
                 value={this.state.todo_description}
                 onChange={this.onChangeToDoDescription}/>
        </div>

        {/*Form html for Responsible text field*/}
        <div className="form-group">
          <label>Responsible: </label>
          <input type="text"
                 className="form-control"
                 value={this.state.todo_responsible}
                 onChange={this.onChangeToDoResponsible}/>
        </div>

        {/*Form html for radio buttons*/}
        <div className="form-group">
        <label>Prioirty : </label>
          <div className="form-check form-check-inline btn-group">
          <label className="btn btn-primary">
            <input style={{display:"none"}} className="form-check-input"

                   type="radio"
                   name="priorityOptions"
                   id="priorityLow"
                   value="Low"
                   checked={this.state.todo_priority==='Low'}
                   onChange={this.onChangeToDoPriority}
                   />Low
            </label>
            </div>

            <div className="form-check form-check-inline btn-group">
            <label className="btn btn-primary">
              <input style={{display:"none"}} className="form-check-input"
                     type="radio"
                     name="priorityOptions"
                     id="priorityMedium"
                     value="Medium"
                     checked={this.state.todo_priority==='Medium'}
                     onChange={this.onChangeToDoPriority}
                     />
                     Medium
              </label>
              </div>

              <div className="form-check form-check-inline btn-group">
              <label className="btn btn-primary">
                <input style={{display:"none"}} className="form-check-input"
                       type="radio"
                       name="priorityOptions"
                       id="prioritHigh"
                       value="High"
                       checked={this.state.todo_priority==='High'}
                       onChange={this.onChangeToDoPriority}
                       />High
                </label>
                </div>

          </div>

        {/*Form Html for a radio button to set completed*/}
        <div className="form-check">
          <input type="checkbox"
                 className="form-check-input"
                 id="completedCheckBox"
                 name="completedCheckBox"
                 onChange={this.onChangeToDoCompleted}
                 checked={this.state.todo_completed}
                 value={this.state.todo_completed}/>
          <label className="form-check-label" htmlFor="completedCheckBox">
            Completed
          </label>
        </div>
        <br/>

        {/*Form html for date and time*/}
        <div className="form-group">
          <label className="col-2 col-form-label">Date and time</label>
          <div className="col-10">
            <input className="form-control" type="datetime-local" value={this.state.todo_date} onChange={this.onChangeToDoDate} id="example-datetime-local-input"/>
          </div>
        </div>

        {/*Submit button html for Form*/}
        <div className="form-group">
          <input type="submit" value="Update" className="btn btn-primary"/>
        </div>
        </form>
      </div>
    )
  }
}
