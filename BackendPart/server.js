const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');

//Getting access to router to set up endpoints
const todoRoutes = express.Router();
const PORT_NUMBER = 4000;

let Todo = require('./todo.model');

app.use(cors());
app.use(bodyParser.json());

mongoose.connect('mongodb://127.0.0.1:27017/todolist', { useNewUrlParser: true });
const connection = mongoose.connection;

connection.once('open', function(){
  console.log("MongoDB database connection successful");
});

//1st Rest endpoint
todoRoutes.route('/').get(function(req, res) {
  Todo.find(function(err, todos){
    if (err) {
      console.log(err);
    } else {
      res.json(todos);
    }
  });
});

//Endpoint Used to extract a single object from the database according to a parameter argument
todoRoutes.route('/:id').get(function(req, res) {
  //First extracting the id from the parameter
  let id = req.params.id;
  Todo.findById(id, function(err, todo) {
    console.log(todo);
    res.json(todo);
  });
});

//Endpoint used to send a HTTP Post request to insert an object in database
todoRoutes.route('/add').post(function(req, res) {
  //extracting the todo item from the request and creating a new object of the data model from it's contents
  let todo = new Todo(req.body);

  todo.save()
    .then(todo => {
      res.status(200).json({'todo': 'todo object added successfully!'});
    })
    .catch(err => {
      res.status(400).send('Adding new todo object failed :('+err);
    });
});

//Rest endpoint to update an existing object in the database
todoRoutes.route('/update/:id').post(function(req, res) {
  Todo.findById(req.params.id, function(err, todo) {
    if(!todo)
      res.status(404).send('Data is not present in database!');
    else
    //if the object to be update is present in the database we take it
    //and fill it up with new values we have recieved in req body to be updated
      todo.todo_description = req.body.todo_description;
      todo.todo_responsible = req.body.todo_responsible;
      todo.todo_priority = req.body.todo_priority;
      todo.todo_completed = req.body.todo_completed;
      todo.todo_date = req.body.todo_date;


      todo.save().then(todo => {
        res.json('Update to the object was successful!');
      })
      .catch(err => {
        res.status(400).send("Update Failed!");
      });
  });
});

  //Rest endpoint to delete an object by extracting the id of the object from url
  todoRoutes.route('/delete/:id').post(function(req, res) {
    Todo.findByIdAndRemove(req.params.id, (err, todo) => {
      if (err) return res.status(500).send(err);
      const response = {
        message: "Todo Successfully Deleted!",
        id: todo._id
      };
      return res.status(200).send(response);
    });
  });

app.use('/todos', todoRoutes);

app.listen(PORT_NUMBER, function(){
  console.log("Server is running on Port: " + PORT_NUMBER);
})
