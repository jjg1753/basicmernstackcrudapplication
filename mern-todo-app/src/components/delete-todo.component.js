import React, {Component} from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

export default class DeleteTodo extends Component {
  state = {user: null}
  _isMounted = false
  constructor(props) {
    super(props);

    axios.post('http://localhost:4000/todos/delete/'+this.props.match.params.id)
      .then(res => {
        if (this._isMounted) {
      this.setState({ user: res.data })
    }
        console.log(res.data)
      });

      this.props.history.push('/');
  }

  componentDidMount() {
      this._isMounted = true
    }
    componentWillUnmount() {
      this._isMounted = false
    }


    render() {
      return(
        <div> Todo Deleted </div>
      )
    }
}
