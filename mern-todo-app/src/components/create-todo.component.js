import React, {Component} from 'react';
import axios from 'axios';

export default class CreateTodo extends Component{
  constructor(props){
    super(props);

    this.onChangeToDoDescription = this.onChangeToDoDescription.bind(this);
    this.onChangeToDoResponsible = this.onChangeToDoResponsible.bind(this);
    this.onChangeToDoPriority = this.onChangeToDoPriority.bind(this);
    this.onChangeToDoDate = this.onChangeToDoDate.bind(this);
    this.onSubmit = this.onSubmit.bind(this);


    this.state = {
      todo_description: '',
      todo_responsible: '',
      todo_priority: '',
      todo_completed: false,
      todo_date: ''
    }
  }

// Here, e is the argument which is the current state
  onChangeToDoDescription(e) {
    this.setState({
      todo_description: e.target.value
    });
  }

  onChangeToDoResponsible(e) {
    this.setState({
      todo_responsible: e.target.value
    });
  }

  onChangeToDoPriority(e) {
    this.setState({
      todo_priority: e.target.value
    });
  }

  onChangeToDoCompleted(e) {
    this.setState({
      todo_completed: e.target.value
    });
  }

  onChangeToDoDate(e) {
    this.setState({
      todo_date: e.target.value
    });
  }

// Clicking on submit event handle
  onSubmit(e){
    e.preventDefault();

    console.log('Form submitted:');
    console.log(`Todo List Description: ${this.state.todo_description}`);
    console.log(`Todo List Responsible: ${this.state.todo_responsible}`);
    console.log(`Todo List Priority: ${this.state.todo_priority}`);
    console.log(`Todo List Completed: ${this.state.todo_completed}`);
    console.log(`Todo List Date: ${this.state.todo_date}`);

    //Creating a new todo object which contains all the contents of the current
    //state and this object will be sent to the backend via http req

    const newTodo = {
      todo_description: this.state.todo_description,
      todo_responsible: this.state.todo_responsible,
      todo_priority: this.state.todo_priority,
      todo_completed: this.state.todo_completed,
      todo_date: this.state.todo_date
    }

    axios.post('http://localhost:4000/todos/add', newTodo)
      .then(res => console.log(res.data));

    this.setState({
      todo_description: '',
      todo_responsible: '',
      todo_priority: '',
      todo_completed: false,
      todo_date: ''
    });
    this.props.history.push('/');
  }

  render(){
    return(
      <div style={{marginTop: 30}}>
        <h3> Create New Todo List by filling out this form </h3>
        <form onSubmit={this.onSubmit}>

        {/*Form Component for Description text field*/}
          <div className="form-group">
            <label>Description: </label>
            <input type="text"
                   className="form-control"
                   value={this.state.todo_description}
                   onChange={this.onChangeToDoDescription}/>
          </div>

          {/*Form html for Responsible text field*/}
          <div className="form-group">
            <label>Responsible: </label>
            <input type="text"
                   className="form-control"
                   value={this.state.todo_responsible}
                   onChange={this.onChangeToDoResponsible}/>
          </div>

          {/*Form html for radio buttons*/}
          <div className="form-group">
          <label>Prioirty : </label>
            <div className="form-check form-check-inline btn-group">
            <label className="btn btn-primary">
              <input style={{display:"none"}} className="form-check-input"

                     type="radio"
                     name="priorityOptions"
                     id="priorityLow"
                     value="Low"
                     checked={this.state.todo_priority==='Low'}
                     onChange={this.onChangeToDoPriority}
                     />Low
              </label>
              </div>

              <div className="form-check form-check-inline btn-group">
              <label className="btn btn-primary">
                <input style={{display:"none"}} className="form-check-input"
                       type="radio"
                       name="priorityOptions"
                       id="priorityMedium"
                       value="Medium"
                       checked={this.state.todo_priority==='Medium'}
                       onChange={this.onChangeToDoPriority}
                       />
                       Medium
                </label>
                </div>

                <div className="form-check form-check-inline btn-group">
                <label className="btn btn-primary">
                  <input style={{display:"none"}} className="form-check-input"
                         type="radio"
                         name="priorityOptions"
                         id="prioritHigh"
                         value="High"
                         checked={this.state.todo_priority==='High'}
                         onChange={this.onChangeToDoPriority}
                         />High
                  </label>
                  </div>

            </div>

          {/*Form html for date and time*/}
          <div className="form-group">
            <label className="col-2 col-form-label">Date and time</label>
            <div className="col-10">
              <input className="form-control" type="datetime-local" value={this.state.todo_date} onChange={this.onChangeToDoDate} id="example-datetime-local-input"/>
            </div>
          </div>

          {/*Submit button html for Form*/}
          <div className="form-group">
            <input type="submit" value="Create Todo" className="btn btn-primary"/>
          </div>
        </form>
      </div>
    )
  }
}
